var development = './dev/';
var production = './prod/';
var assets = 'assets/';
var images = 'images/';
var fonts = 'fonts/';
var lib = 'lib';


var gulp = require('gulp'),
    imagemin = require('gulp-imagemin'),
    imageminOptipng = require('imagemin-optipng'),
    imageminPngquant = require('imagemin-pngquant'),
    htmlmin = require('gulp-htmlmin'),
    useref = require('gulp-useref'),
    gulpif = require('gulp-if'),
    uglify = require('gulp-uglify'),
    minifyCss = require('gulp-clean-css');

gulp.task('html-js-css', function () {
    return gulp.src(development + '*.html')
        .pipe(useref())
        .pipe(gulpif('*.js', uglify({
            beautify: true,
            comments: false,
            sourceMap: false,
            mangle: false
        })))
        .pipe(gulpif('*.css', minifyCss()))
        .pipe(gulpif('*.html',htmlmin({
            collapseWhitespace: true,
            removeComments: true
          }
        )))

        .pipe(gulp.dest(production));
});

gulp.task('images', function() {
    gulp.src(development + assets +'**/*.{jpg,png,gif,svg}')
        .pipe(imagemin(
            {
                use: imageminPngquant({quality: '65-80'})
            }
))
        .pipe(gulp.dest(production + assets));

     gulp.src(development + images +'**/*.{jpg,png,gif,svg}')
         .pipe(imagemin({
             progressive: false
         }))
         .pipe(gulp.dest(production + images));
});

gulp.task('fonts', function() {
    gulp.src(development + assets + fonts + '**/*')
        .pipe(gulp.dest(production + assets + fonts));
});

gulp.task('lib', function() {
    gulp.src(development + lib + '**/*')
        .pipe(gulp.dest(production));
});


gulp.task('compress', ['html-js-css', 'fonts', 'images', 'lib']);


