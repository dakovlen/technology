$(document).on('ready', function() {

    $(window).load(function () {
        $(".load-container").delay(150).fadeOut(250);
    });
    $("head").append('<link href="https://fonts.googleapis.com/css?family=Dancing+Script" rel="stylesheet">');





    $('.single-item').slick({
        autoplay: true,
        autoplaySpeed: 5000,
        speed: 500,
        dots:true,
        arrows: false
    });
    
    
    $('.button').click(function(){

        var value = $(this).attr('id');


        $(".all").fadeOut('slow');
        $('.'+value).fadeIn('slow');
    });
    

   var $scrollMenu = $("#scroll-menu");

    $(window).scroll(function(){

        if ( $(this).scrollTop() > 50 && $scrollMenu.hasClass("default") ){

            $scrollMenu.fadeOut('fast',function(){
                $(this).removeClass("default")
                    .addClass("fixed")
                    .fadeIn('slow');
            });
        } else if($(this).scrollTop() <= 50 && $scrollMenu.hasClass("fixed")) {
            $scrollMenu.fadeOut('fast',function(){
                $(this).removeClass("fixed transbg")
                    .addClass("default")
                    .fadeIn('slow');
            });
        }
    });
    

    $('a[href^="#"]').click(function(){
        var target = $(this).attr('href');
        $('html, body').animate({scrollTop: $(target).offset().top-40}, 800);
        return false;
    });
    

    $("#back-top").hide();
    $(function () {
        $(window).scroll(function () {
            if ($(this).scrollTop() > 50) {
                $('#back-top').fadeIn();
            } else {
                $('#back-top').fadeOut();
            }
        });
        $('#back-top a').click(function () {
            $('body,html').animate({
                scrollTop: 0
            }, 800);
            return false;
        });
    });



    onScrollInit( $('.os-animation') );

    function onScrollInit( items, trigger ) {
        items.each( function() {
            var osElement = $(this),
                osAnimationClass = osElement.attr('data-os-animation'),
                osAnimationDelay = osElement.attr('data-os-animation-delay');

            osElement.css({
                '-webkit-animation-delay':  osAnimationDelay,
                '-moz-animation-delay':     osAnimationDelay,
                'animation-delay':          osAnimationDelay
            });

            var osTrigger = ( trigger ) ? trigger : osElement;

            osTrigger.waypoint(function() {
                osElement.addClass('animated').addClass(osAnimationClass);
            },{
                triggerOnce: true,
                offset: '80%'
            });
        });
    }







});




